﻿using DiscreteEventSimulator;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace DiscreteEventSimulatorTesting
{
    
    
    /// <summary>
    ///This is a test class for GovernorTest and is intended
    ///to contain all GovernorTest Unit Tests
    ///</summary>
    [TestClass()]
    public class GovernorTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Governor Constructor
        ///</summary>
        [TestMethod()]
        public void GovernorConstructorTest()
        {
            Governor target = new Governor();

            Assert.IsInstanceOfType(target, typeof(Governor));
        }

        /// <summary>
        ///A test for AddNewProductType
        ///</summary>
        [TestMethod()]
        public void AddNewProductTypeTest()
        {
            Governor target = new Governor();
            string typeName = "Test";
            target.AddNewProductType(typeName);

            Assert.AreEqual(typeName, target.ProductTypes[0].TypeName);
        }

        /// <summary>
        ///A test for AddNewProductType with a null string
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNewProductTypeTestNullTypeName()
        {
            Governor target = new Governor();
            string typeName = null;
            target.AddNewProductType(typeName);

            Assert.AreEqual(typeName, target.ProductTypes[0].TypeName);
        }

        /// <summary>
        ///A test for AddNewProductType with a empty string
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNewProductTypeTestEmptyTypeName()
        {
            Governor target = new Governor();
            string typeName = string.Empty;
            target.AddNewProductType(typeName);

            Assert.AreEqual(typeName, target.ProductTypes[0].TypeName);
        }

        /// <summary>
        ///A test for AddNewProductType with a duplicate typename
        ///</summary>
        [TestMethod()]
        public void AddNewProductTypeTestDuplicateTypeName()
        {
            Governor target = new Governor();
            string typeName = "Test";
            target.AddNewProductType(typeName);
            target.AddNewProductType(typeName);

            Assert.AreEqual(typeName, target.ProductTypes[0].TypeName);
            Assert.AreEqual(1, target.ProductTypes.Count);
        }

        /// <summary>
        ///A test for AddNewRepType
        ///</summary>
        [TestMethod()]
        public void AddNewRepTypeTest()
        {
            Governor target = new Governor();
            string typeName = "Test"; 
            target.AddNewRepType(typeName);
            List<SalesRepType> keys = new List<SalesRepType>(target.RepNums.Keys);
            Assert.AreEqual("Test", keys[0].TypeName);
            Assert.AreEqual(1, target.RepNums[keys[0]]);
        }

        /// <summary>
        ///A test for AddNewRepType null string
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNewRepTypeTestNullString()
        {
            Governor target = new Governor();
            string typeName = null;
            target.AddNewRepType(typeName);
        }

        /// <summary>
        ///A test for AddNewRepType empty string
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddNewRepTypeTestEmptyString()
        {
            Governor target = new Governor();
            string typeName = string.Empty;
            target.AddNewRepType(typeName);
        }

        /// <summary>
        ///A test for AddNewRepType duplicate string
        ///</summary>
        [TestMethod()]
        public void AddNewRepTypeTestDuplicateTypeName()
        {
            Governor target = new Governor();
            string typeName = "Test";
            target.AddNewRepType(typeName);
            target.AddNewRepType(typeName);
            List<SalesRepType> keys = new List<SalesRepType>(target.RepNums.Keys);
            Assert.AreEqual("Test", keys[0].TypeName);
            Assert.AreEqual(1, target.RepNums[keys[0]]);
            Assert.AreEqual(1, target.RepNums.Count);
        }

        /// <summary>
        ///A test for AddRepTypeHandle
        ///</summary>
        [TestMethod()]
        public void AddRepTypeHandleTest()
        {
            Governor target = new Governor();
            SalesRepType repType = new SalesRepType("Test");
            ProductType toHandle = new ProductType("Test", 0.1, 0.1);
            target.RepNums.Add(repType, 1);
            target.AddRepTypeHandle(repType, toHandle);
            List<SalesRepType> keys = new List<SalesRepType>(target.RepNums.Keys);
            Assert.AreEqual(toHandle, keys[0].Handles[0]);
        }

        /// <summary>
        ///A test for AddRepTypeHandle with a null RepType
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddRepTypeHandleTestNullSalesRepType()
        {
            Governor target = new Governor();
            SalesRepType repType = null;
            ProductType toHandle = new ProductType("Test", 0.1, 0.1);
            target.AddRepTypeHandle(repType, toHandle);
        }

        /// <summary>
        ///A test for AddRepTypeHandle with a null ProductType
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddRepTypeHandleTestNullProductType()
        {
            Governor target = new Governor();
            SalesRepType repType = new SalesRepType("Test");
            target.RepNums.Add(repType, 1);
            ProductType toHandle = null;
            target.AddRepTypeHandle(repType, toHandle);
        }

        /// <summary>
        ///A test for AddRepTypeHandle with a SalesRepType that is not in the Dictionary
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void AddRepTypeHandleTestRepTypeNotInDictionary()
        {
            Governor target = new Governor();
            SalesRepType repType = new SalesRepType("Test");
            ProductType toHandle = new ProductType("Test", 0.1, 0.1);
            //target.RepNums.Add(repType, 1); // Dont add to dictionary
            target.AddRepTypeHandle(repType, toHandle);
        }

        /// <summary>
        ///A test for IsSimulationValid invalid
        ///</summary>
        [TestMethod()]
        public void IsSimulationValidTestInvalid()
        {
            Governor target = new Governor();
            bool expected = false;
            bool actual;
            actual = target.IsSimulationValid();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for IsSimulationValid valid
        ///</summary>
        [TestMethod()]
        public void IsSimulationValidTestValid()
        {
            Governor target = new Governor();
            bool expected = true;
            SalesRepType srt = new SalesRepType("Test");
            target.RepNums.Add(srt, 1);
            target.ProductTypes.Add(new ProductType("Test", 0.1, 1));
            List<SalesRepType> keys = new List<SalesRepType>(target.RepNums.Keys);
            keys[0].Handles.Add(target.ProductTypes[0]);
            bool actual;
            actual = target.IsSimulationValid();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for RemoveProductType with a product type that is in the list
        ///</summary>
        [TestMethod()]
        public void RemoveProductTypeTest()
        {
            Governor target = new Governor();
            ProductType type = new ProductType("Test", 0.1, 0.1);
            target.ProductTypes.Add(type);
            target.RemoveProductType(type);

            Assert.AreEqual(0, target.ProductTypes.Count);
        }

        /// <summary>
        ///A test for RemoveProductType with a null product type
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveProductTypeTestNullProductType()
        {
            Governor target = new Governor();
            ProductType type = null;
            target.RemoveProductType(type);
        }

        /// <summary>
        ///A test for RemoveProductType with a product type that is not in the list
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveProductTypeTestProductTypeNotInList()
        {
            Governor target = new Governor();
            ProductType type = new ProductType("Test", 0.1, 0.1);
            target.RemoveProductType(type);
        }

        /// <summary>
        ///A test for RemoveRepType
        ///</summary>
        [TestMethod()]
        public void RemoveRepTypeTest()
        {
            Governor target = new Governor();
            SalesRepType type = new SalesRepType("Test");
            target.RepNums.Add(type, 1);
            target.RemoveRepType(type);
            Assert.AreEqual(0, target.RepNums.Count);
        }

        /// <summary>
        ///A test for RemoveRepType with a null SalesRepType
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveRepTypeTestNullRepType()
        {
            Governor target = new Governor();
            SalesRepType type = null;
            target.RemoveRepType(type);
        }

        /// <summary>
        ///A test for RemoveRepType with a SalesRepType that is not in the dictionary
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveRepTypeTestSalesRepTypeNotInDictionary()
        {
            Governor target = new Governor();
            SalesRepType type = new SalesRepType("Test");
            target.RemoveRepType(type);
        }

        /// <summary>
        ///A test for RemoveRepTypeHandle
        ///</summary>
        [TestMethod()]
        public void RemoveRepTypeHandleTest()
        {
            Governor target = new Governor();
            SalesRepType repType = new SalesRepType("Test");
            ProductType toRemove = new ProductType("Test", 0.1, 0.1);
            repType.Handles.Add(toRemove);
            target.RepNums.Add(repType, 1);
            target.RemoveRepTypeHandle(repType, toRemove);

            Assert.AreEqual(0, repType.Handles.Count);
        }

        /// <summary>
        ///A test for RemoveRepTypeHandle null SalesRepType
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveRepTypeHandleTestNullSalesRepType()
        {
            Governor target = new Governor();
            SalesRepType repType = null;
            ProductType toRemove = new ProductType("Test", 0.1, 0.1);
            target.RemoveRepTypeHandle(repType, toRemove);
        }

        /// <summary>
        ///A test for RemoveRepTypeHandle null ProductType
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveRepTypeHandleTestNullProductType()
        {
            Governor target = new Governor();
            SalesRepType repType = new SalesRepType("Test");
            ProductType toRemove = null;
            target.RemoveRepTypeHandle(repType, toRemove);
        }

        /// <summary>
        ///A test for RemoveRepTypeHandle SalesRepType not in dictionary
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveRepTypeHandleTestSalesRepTypeNotInDictionary()
        {
            Governor target = new Governor();
            SalesRepType repType = new SalesRepType("Test");
            //Dont add the reptype to the dictionary
            ProductType toRemove = new ProductType("Test", 0.1, 0.1);
            target.RemoveRepTypeHandle(repType, toRemove);
        }

        /// <summary>
        ///A test for RemoveRepTypeHandle SalesRepType not in dictionary
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void RemoveRepTypeHandleTestProductTypeNotInHandlesList()
        {
            Governor target = new Governor();
            SalesRepType repType = new SalesRepType("Test");
            target.RepNums.Add(repType, 1);
            ProductType toRemove = new ProductType("Dave", 0.1, 0.1);
            target.RemoveRepTypeHandle(repType, toRemove);
        }

        /// <summary>
        ///A test for Reset
        ///</summary>
        [TestMethod()]
        public void ResetTest()
        {
            Governor target = new Governor();
            DateTime now = DateTime.Now;
            target.BeginTime = now;
            target.Reset();
            Assert.AreNotEqual(now, target.BeginTime);
        }

        /// <summary>
        ///A test for SetProductTypeSettings
        ///</summary>
        [TestMethod()]
        public void SetProductTypeSettingsTest()
        {
            Governor target = new Governor();
            ProductType pt = new ProductType("Test", 0.1, 0.1);
            target.ProductTypes.Add(pt);
            string typeName = "Test"; 
            double probability = 1.0; 
            double processingMultiplier = 1.0; 
            target.SetProductTypeSettings(typeName, probability, processingMultiplier);

            Assert.AreEqual(probability, pt.ProductTypeProbability);
            Assert.AreEqual(processingMultiplier, pt.ProcessingDelayMultiplier);
        }

        /// <summary>
        ///A test for SetProductTypeSettings null string
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetProductTypeSettingsTestNullString()
        {
            Governor target = new Governor();
            ProductType pt = new ProductType("Test", 0.1, 0.1);
            target.ProductTypes.Add(pt);
            string typeName = null;
            double probability = 1.0;
            double processingMultiplier = 1.0;
            target.SetProductTypeSettings(typeName, probability, processingMultiplier);
        }

        /// <summary>
        ///A test for SetProductTypeSettings empty string
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void SetProductTypeSettingsTestEmptyString()
        {
            Governor target = new Governor();
            ProductType pt = new ProductType("Test", 0.1, 0.1);
            target.ProductTypes.Add(pt);
            string typeName = string.Empty;
            double probability = 1.0;
            double processingMultiplier = 1.0;
            target.SetProductTypeSettings(typeName, probability, processingMultiplier);
        }

        /// <summary>
        ///A test for SetProductTypeSettings typename that has no matching type
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void SetProductTypeSettingsTestNoMatchingTypeName()
        {
            Governor target = new Governor();
            ProductType pt = new ProductType("Test", 0.1, 0.1);
            target.ProductTypes.Add(pt);
            string typeName = "Dave";
            double probability = 1.0;
            double processingMultiplier = 1.0;
            target.SetProductTypeSettings(typeName, probability, processingMultiplier);
        }

        /// <summary>
        ///A test for TotalProbability
        ///</summary>
        [TestMethod()]
        public void TotalProbabilityTest()
        {
            Governor target = new Governor();
            target.ProductTypes.Add(new ProductType("Test", 0.1, 0.16));
            target.ProductTypes.Add(new ProductType("Test2", 0.2, 0.16));
            double expected = 0.32; 
            double actual;
            actual = target.TotalProbability();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for TotalProbability with no product types
        ///</summary>
        [TestMethod()]
        public void TotalProbabilityTestNoProductTypes()
        {
            Governor target = new Governor();
            double expected = 0;
            double actual;
            actual = target.TotalProbability();
            Assert.AreEqual(expected, actual);
        }
    }
}
